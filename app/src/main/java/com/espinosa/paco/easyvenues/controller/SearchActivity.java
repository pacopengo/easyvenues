package com.espinosa.paco.easyvenues.controller;

import com.espinosa.paco.easyvenues.R;
import com.espinosa.paco.easyvenues.model.VenueData;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

public class SearchActivity extends AppCompatActivity {

    // the foursquare client_id and the client_secret

    // ============== YOU SHOULD MAKE NEW KEYS ====================//
    final String CLIENT_ID = "5DTN3XSPGSKNGEF02OG5YNDA02KG44CRDPPCQOA4YBPNI1YY";
    final String CLIENT_SECRET = "1B1N3ZWJ1P4B0S4SBRUUYQAGPBQFP2B5TB2YQV433KFIO0V5";

    // we will need to take the latitude and the logntitude from a certain point
    // this is the center of New York
    final String latitude = "19.4135058";// "40.7463956";
    final String longtitude = "-99.1781033"; //"-73.9852992";

    ArrayAdapter<String> adaptador;
    ListView lstLista;
    static ArrayList<VenueData> venuesList;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        getDatos();

    }


    /**
     * Method gets triggered when Login button is clicked
     *
     */
    public void getDatos()
    {

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // Get Email Edit View Value

        // Put Http parameter username with value of Email Edit View control
        params.put("client_id", CLIENT_ID);
        // Put Http parameter password with value of Password Edit Value control
        params.put("client_secret", CLIENT_SECRET);
        params.put("v", "20130815");
        params.put("ll", latitude+","+longtitude);
        params.put("query", getIntent().getExtras().getString("query"));

        // Invoke RESTful Web Service with Http parameters
        invokeAPI(params);
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeAPI(RequestParams params)
    {
        // Show Progress Dialog
        //prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.post("https://api.foursquare.com/v2/venues/search",params ,new AsyncHttpResponseHandler()
        {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response)
            {
                //String temp = parseFoursquare(response);
                venuesList = (ArrayList<VenueData>) parseFoursquare(response);


                if (venuesList == null)
                {
                    // we have an error to the call
                    // we can also stop the progress bar
                }

                else
                {

                    List<String> listTitle = new ArrayList<String>();

                    for (int i = 0; i < venuesList.size(); i++)
                    {
                        // make a list of the venus that are loaded in the list.
                        // show the name, the category and the city
                        listTitle.add(i, venuesList.get(i).getName() + ", " + venuesList.get(i).getCategory() + ", " + venuesList.get(i).getCity());
                    }

                    // set the results to the list
                    // and show them in the xml
                    //adaptador = new ArrayAdapter(FourSquareActivity.this, R.layout.row_layout, R.id.listText, listTitle);

                    adaptador = new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_list_item_1, listTitle);
                    lstLista = (ListView)findViewById(R.id.lstList);//(ListView)android.R.id.list;
                    lstLista.setAdapter(adaptador);

                    lstLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            navigatetoVenueActivity(position);
                        }
                    });

                }
            }


            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content)
            {
                // Hide Progress Dialog
                //prgDialog.hide();

                // When Http response code is '404'
                if(statusCode == 404)
                {
                    Toast.makeText(getApplicationContext(), "El recurso ya no existe o se movió de lugar", Toast.LENGTH_LONG).show();
                }

                // When Http response code is '500'
                else if(statusCode == 500)
                {
                    Toast.makeText(getApplicationContext(), "Ups! Existe un error con el servidor. Intente de nuevo más tarde o contacte al administrador del servicio", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else
                {
                    Toast.makeText(getApplicationContext(), "¡Ha ocurrido un error inesperado! [Error más común: El dispositivo no está conectado a internet o el servidor remoto no está corriendo]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoVenueActivity(int position)
    {
        Intent intent = new Intent(getApplicationContext(),VenueActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("position", position);
        intent.putExtra("activity", 0);
        startActivity(intent);
    }

    private static ArrayList<VenueData> parseFoursquare(String response) {

        ArrayList<VenueData> lista = new ArrayList<VenueData>();
        try {
            // make an jsonObject in order to parse the response
            JSONObject jsonObject = new JSONObject(response);


            // make an jsonObject in order to parse the response
            if (jsonObject.has("response")) {
                if (jsonObject.getJSONObject("response").has("venues")) {
                    JSONArray jsonArray = jsonObject.getJSONObject("response").getJSONArray("venues");



                    for (int i = 0; i < jsonArray.length(); i++) {
                        VenueData v = new VenueData();
                        if(jsonArray.getJSONObject(i).has("id"))
                        {
                            v.setId(jsonArray.getJSONObject(i).getString("id"));
                        }
                        if (jsonArray.getJSONObject(i).has("name")) {
                            v.setName(jsonArray.getJSONObject(i).getString("name"));

                            if (jsonArray.getJSONObject(i).has("location")) {
                                if (jsonArray.getJSONObject(i).getJSONObject("location").has("address")) {
                                    v.setAddress(jsonArray.getJSONObject(i).getJSONObject("location").getString("address"));
                                    if (jsonArray.getJSONObject(i).getJSONObject("location").has("city")) {
                                        v.setCity(jsonArray.getJSONObject(i).getJSONObject("location").getString("city"));
                                    }

                                    if (jsonArray.getJSONObject(i).getJSONObject("location").has("state")) {
                                        v.setState(jsonArray.getJSONObject(i).getJSONObject("location").getString("state"));
                                    }

                                    if (jsonArray.getJSONObject(i).getJSONObject("location").has("postalCode")) {
                                        v.setZip(jsonArray.getJSONObject(i).getJSONObject("location").getString("postalCode"));
                                    }


                                    if (jsonArray.getJSONObject(i).has("categories")) {
                                        if (jsonArray.getJSONObject(i).getJSONArray("categories").length() > 0) {
                                            if (jsonArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).has("icon")) {
                                                v.setCategory(jsonArray.getJSONObject(i).getJSONArray("categories").getJSONObject(0).getString("name"));
                                            }
                                        }
                                    }
                                }
                            }


                            if (jsonArray.getJSONObject(i).has("contact")) {
                                if (jsonArray.getJSONObject(i).getJSONObject("contact").has("formattedPhone")) {
                                    v.setTelephone(jsonArray.getJSONObject(i).getJSONObject("contact").getString("formattedPhone"));
                                }
                            }

                            lista.add(v);
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<VenueData>();
        }
        return lista;

    }
}
