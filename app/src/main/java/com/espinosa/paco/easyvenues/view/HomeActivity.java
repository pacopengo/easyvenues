package com.espinosa.paco.easyvenues.view;

import com.espinosa.paco.easyvenues.R;
import com.espinosa.paco.easyvenues.controller.DonateActivity;
import com.espinosa.paco.easyvenues.controller.FavoriteActivity;
import com.espinosa.paco.easyvenues.controller.SearchActivity;

import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class HomeActivity extends AppCompatActivity
{

    EditText search;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

       getWindow().getDecorView().setBackgroundColor(Color.rgb(77, 148, 255));
        // Find Email Edit View control by ID
        search = (EditText)findViewById(R.id.txtSearch);
    }


    public void searchQuery(View view)
    {
        // Get Email Edit View Value
        String query = search.getText().toString();

        Intent intent = new Intent(getApplicationContext(),SearchActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("query", query);
        startActivity(intent);

    }

    public void navigateToFavorites(View view)
    {
        Intent intent = new Intent(getApplicationContext(),FavoriteActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    public void navigateToDonate(View view)
    {
        Intent intent = new Intent(getApplicationContext(),DonateActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}