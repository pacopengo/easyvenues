package com.espinosa.paco.easyvenues.controller;

import com.espinosa.paco.easyvenues.R;
import com.espinosa.paco.easyvenues.helpers.ValidateUtility;
import com.espinosa.paco.easyvenues.view.HomeActivity;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

/**
 *
 * Login Activity Class
 *
 */
public class LoginActivity extends Activity
{

    // Progress Dialog Object
    ProgressDialog prgDialog;

    // Error Msg TextView Object
    TextView lblError;

    // Email Edit View Object
    EditText txtEmail;

    // Passwprd Edit View Object
    EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        getWindow().getDecorView().setBackgroundColor(Color.rgb(255, 51, 51));

        // Find Error Msg Text View control by ID
        lblError = (TextView)findViewById(R.id.lblError);

        // Find Email Edit View control by ID
        txtEmail = (EditText)findViewById(R.id.txtEmail);

        // Find Password Edit View control by ID
        txtPassword = (EditText)findViewById(R.id.txtPassword);

        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);

        // Set Progress Dialog Text
        prgDialog.setMessage("Por favor, espere...");

        // Set Cancelable as False
        prgDialog.setCancelable(false);
    }


    /**
     * Method gets triggered when Login button is clicked
     *
     * @param view
     */
    public void loginUser(View view)
    {
        // Get Email Edit View Value
        String email = txtEmail.getText().toString();

        // Get Password Edit View Value
        String password = txtPassword.getText().toString();

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();

        // When Email Edit View and Password Edit View have values other than Null
        if(ValidateUtility.isNotNull(email) && ValidateUtility.isNotNull(password))
        {
            // When Email entered is Valid
            if(ValidateUtility.validate(email))
            {
                // Put Http parameter username with value of Email Edit View control
                params.put("a1", email);
                // Put Http parameter password with value of Password Edit Value control
                params.put("a2", password);
                // Invoke RESTful Web Service with Http parameters
                invokeWS(params);
            }

            // When Email is invalid
            else
            {
                Toast.makeText(getApplicationContext(), "Por favor, ingrese un email válido", Toast.LENGTH_LONG).show();
            }
        }

        else
        {
            Toast.makeText(getApplicationContext(), "Por favor, rellene todos los campos del formulario", Toast.LENGTH_LONG).show();
        }

    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
    public void invokeWS(RequestParams params)
    {
        // Show Progress Dialog
        prgDialog.show();

        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        client.post("http://developer.heanan.com/android/datos.php",params ,new AsyncHttpResponseHandler()
        {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(String response)
            {
                // Hide Progress Dialog
                prgDialog.hide();
                try
                {
                    // JSON Object
                    JSONObject obj = new JSONObject(response);

                    String status = obj.getJSONObject("response").getString("success");

                    if(status.equals("OK"))
                    {
                        Toast.makeText(getApplicationContext(), "Has iniciado sesión correctamente!!", Toast.LENGTH_LONG).show();
                        // Navigate to Home screen
                        navigatetoHomeActivity();
                    }

                    // Else display error message
                    else
                    {
                        //errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), "Los datos son incorrectos, intente nuevamente", Toast.LENGTH_LONG).show();
                    }
                }

                catch (JSONException e)
                {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error ocurrido [La respuesta JSON del servidor es inválida]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, Throwable error, String content)
            {
                // Hide Progress Dialog
                prgDialog.hide();

                // When Http response code is '404'
                if(statusCode == 404)
                {
                    Toast.makeText(getApplicationContext(), "El recurso ya no existe o se movió de lugar", Toast.LENGTH_LONG).show();
                }

                // When Http response code is '500'
                else if(statusCode == 500)
                {
                    Toast.makeText(getApplicationContext(), "Ups! Existe un error con el servidor. Intente de nuevo más tarde o contacte al administrador del servicio", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else
                {
                    Toast.makeText(getApplicationContext(), "¡Ha ocurrido un error inesperado! [Error más común: El dispositivo no está conectado a internet o el servidor remoto no está corriendo]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoHomeActivity()
    {
        Intent homeIntent = new Intent(getApplicationContext(),HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }


}
