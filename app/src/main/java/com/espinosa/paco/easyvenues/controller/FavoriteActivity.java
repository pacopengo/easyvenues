package com.espinosa.paco.easyvenues.controller;

import com.espinosa.paco.easyvenues.R;
import com.espinosa.paco.easyvenues.model.DBConnector;
import com.espinosa.paco.easyvenues.model.VenueData;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class FavoriteActivity extends AppCompatActivity {

    SQLiteDatabase db;
    TextView txtFav;
    ListView lstListaFavs;
    ArrayAdapter<String> adaptador;
    static ArrayList<VenueData> venuesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);

        txtFav = (TextView) findViewById(R.id.txtListaFavs);
        lstListaFavs = (ListView) findViewById(R.id.lstListaFavs);

        //Abrimos la base de datos 'DBUsuarios' en modo escritura
        DBConnector con = new DBConnector(this, "DB_Test", null, 1);
        db = con.getWritableDatabase();

        getFavorites();
    }

    private void getFavorites()
    {
        try
        {
            if (db != null) {
                //Alternativa 1: método rawQuery()
                Cursor c = db.rawQuery("SELECT * FROM favoritos", null);
                List<String> lista = new ArrayList<String>();
                venuesList = new ArrayList<VenueData>();

                if (c.getCount() > 0) {
                    //Alternativa 2: método delete()
                    //String[] campos = new String[] {"codigo", "nombre"};
                    //Cursor c = db.query("Usuarios", campos, null, null, null, null, null);

                    if (c.moveToFirst()) {

                        int i= 0;
                        //Recorremos el cursor hasta que no haya más registros
                        do {
                            VenueData v= new VenueData();
                            v.setId(c.getString(0));
                            v.setName(c.getString(1));
                            v.setAddress(c.getString(2));
                            v.setZip(c.getString(3));
                            v.setCity(c.getString(4));
                            v.setState(c.getString(5));
                            v.setTelephone(c.getString(6));
                            venuesList.add(v);
                            lista.add(v.getName() + ", "+v.getCity());
                            i++;

                        } while (c.moveToNext());
                    }

                    adaptador = new ArrayAdapter<String>(FavoriteActivity.this, android.R.layout.simple_list_item_1, lista);
                    lstListaFavs = (ListView)findViewById(R.id.lstListaFavs);//(ListView)android.R.id.list;
                    lstListaFavs.setAdapter(adaptador);

                    lstListaFavs.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        public void onItemClick(AdapterView<?> parent, View view,
                                                int position, long id) {
                            Toast.makeText(getApplicationContext(), ""+position, Toast.LENGTH_LONG).show();
                            navigatetoVenueActivity(position);
                        }
                    });
                } else {
                    txtFav.setText("Aún no ha agregado elementos a \"Mis favoritos\"");
                }

                    // set the results to the list
                    // and show them in the xml
                    //adaptador = new ArrayAdapter(FourSquareActivity.this, R.layout.row_layout, R.id.listText, listTitle);


            }
        }

        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error al obtener los favoritos desde la Base de Datos: \n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoVenueActivity(int position)
    {
        Intent intent = new Intent(getApplicationContext(),VenueActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("position", position);
        intent.putExtra("activity", 1);
        startActivity(intent);
    }
}
