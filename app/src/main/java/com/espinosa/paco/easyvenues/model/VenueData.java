package com.espinosa.paco.easyvenues.model;

/**
 * Created by root on 26/05/16.
 */
public class VenueData
{
    private String name;
    private String city;
    private String address;
    private String zip;
    private String state;
    private String telephone;
    private String id;

    private String category;

    public VenueData()
    {
        this.name = "";
        this.city = "";
        this.address="";
        this.zip="";
        this.state="";
        this.telephone="";
        this.setCategory("");
    }

    public String getCity()
    {
        if (city.length() > 0)
        {
            return city;
        }
        return city;
    }

    public void setCity(String city)
    {
        if (city != null)
        {
            this.city = city.replaceAll("\\(", "").replaceAll("\\)", "");
            ;
        }
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getName()
    {
        return name;
    }

    public String getCategory()
    {
        return category;
    }

    public void setCategory(String category)
    {
        this.category = category;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
