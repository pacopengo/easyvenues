package com.espinosa.paco.easyvenues.controller;

import com.espinosa.paco.easyvenues.R;
import com.espinosa.paco.easyvenues.model.DBConnector;
import com.espinosa.paco.easyvenues.model.VenueData;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.content.ContentValues;

import java.util.ArrayList;

public class VenueActivity extends AppCompatActivity {

    TextView txtNombre, txtDetails;
    Switch btnSwitch;
    int i; //Posición actual
    ArrayList<VenueData> venuesList;

    String id, name, address, zipCode, city, state, telephone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_venue);
        venuesList = null;
        id=""; name=""; address=""; zipCode=""; city=""; state=""; telephone="";
        txtNombre = (TextView)findViewById(R.id.txtNombre);
        txtDetails = (TextView)findViewById(R.id.txtDetails);
        btnSwitch = (Switch) findViewById(R.id.btnSwitch);
        i = getIntent().getExtras().getInt("position");

        checkClass();
        getData();
        isFavorite();
        checkFavStatus();

    }


    private void getData()
    {

        try
        {
            txtNombre.setText(venuesList.get(i).getName());

            String detalles = "";

            name = txtNombre.getText().toString();
            id = venuesList.get(i).getId();
            address = venuesList.get(i).getAddress();
            zipCode = venuesList.get(i).getZip();
            city = venuesList.get(i).getCity();
            state = venuesList.get(i).getState();
            telephone = venuesList.get(i).getTelephone();

            Toast.makeText(getApplicationContext(), telephone, Toast.LENGTH_LONG).show();

            if(address!="")
                detalles=address;

            if(zipCode!="")
                detalles+="\n"+zipCode;

            if(city!="")
                detalles+="\n"+city;

            if(state!="")
                detalles+="\n"+state;

            if(telephone!="")
                detalles+="\n"+telephone;

            if(detalles=="")
                detalles = "No hay detalles sobre esta ubicación";

            txtDetails.setText(detalles+"\n\n");
        }

        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error al obtener los datos: \n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private void checkFavStatus()
    {
        btnSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if(isChecked)
                {
                    addFavorite();
                }

                else
                {
                    deleteFavorite();
                }

            }
        });
    }


    private void addFavorite()
    {
        try
        {
            //Abrimos la base de datos 'DBUsuarios' en modo escritura
            DBConnector con = new DBConnector(this, "DB_Test", null, 1);
            SQLiteDatabase db = con.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                //Insertamos los datos en la tabla Usuarios
                //db.execSQL("INSERT INTO favoritos (id, name, address, zip_code, city, state, telephone) " + "VALUES (\"" + id + "\", \"" + txtNombre.getText() + "\", \"" + address + "\", \"" + zipCode + "\", \"" + city + "\", \"" + state + "\", \"" + telephone +"\")");

                //Alternativa 2: método insert()
                ContentValues nuevoFavorito = new ContentValues();
                nuevoFavorito.put("id", id);
                nuevoFavorito.put("name", name);
                nuevoFavorito.put("address", address);
                nuevoFavorito.put("zip_code", zipCode);
                nuevoFavorito.put("city", city);
                nuevoFavorito.put("state", state);
                nuevoFavorito.put("telephone", telephone);
                db.insert("favoritos", null, nuevoFavorito);

                Toast.makeText(getApplicationContext(), "Se agregó "+ name +" a Mis Favoritos", Toast.LENGTH_LONG).show();
            }

            //Cerramos la base de datos
            db.close();
        }
        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error al insertar el favorito en la Base de Datos: \n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void deleteFavorite()
    {
        try
        {
            //Abrimos la base de datos 'DBUsuarios' en modo escritura
            DBConnector con = new DBConnector(this, "DB_Test", null, 1);
            SQLiteDatabase db = con.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                //Alternativa 2: método delete()
                db.delete("favoritos", "id=?", new String[] { id });
                Toast.makeText(getApplicationContext(), "Se eliminó "+ name +" de Mis Favoritos", Toast.LENGTH_LONG).show();
            }

            //Cerramos la base de datos
            db.close();
        }
        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error al eliminar el favorito de la Base de Datos: \n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }

    }

    private void isFavorite()
    {
        try
        {
            //Abrimos la base de datos 'DBUsuarios' en modo escritura
            DBConnector con = new DBConnector(this, "DB_Test", null, 1);
            SQLiteDatabase db = con.getWritableDatabase();

            //Si hemos abierto correctamente la base de datos
            if(db != null)
            {
                //Alternativa 1: método rawQuery()
                Cursor c = db.rawQuery("SELECT * FROM favoritos WHERE id = \"" + id + "\"", null);

                if (c.getCount() > 0) {
                    btnSwitch.setChecked(true);
                } else {
                    btnSwitch.setChecked(false);
                }
            }
        }

        catch(Exception e)
        {
            Toast.makeText(getApplicationContext(), "Ha ocurrido un error al obtener el estátus del venue actual: \n" + e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    private void checkClass()
    {
        if(getIntent().getExtras().getInt("activity")==0)
        {
            venuesList = SearchActivity.venuesList;
        }

        else
        {
            venuesList = FavoriteActivity.venuesList;
        }
    }
}

